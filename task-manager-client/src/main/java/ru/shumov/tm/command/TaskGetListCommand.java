package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.task.Task;
import ru.shumov.tm.endpoint.user.Role;

import java.util.Collection;

public class TaskGetListCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "task list";
    @Getter
    private final String description = "task list: Вывод всех задач.";
    @SneakyThrows
    @Override
    public void execute() {
        var counter = 0;
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            Collection<Task> values = bootstrap.getTaskEndPoint().getList(session);
            if (values.isEmpty()) {
                bootstrap.getTerminalService().outPutString(Constants.TASKS_DO_NOT_EXIST);
                return;
            }
            for (Task task : values) {
                if (task.getUserId().equals(bootstrap.getUser().getId())) {
                    bootstrap.getTerminalService()
                            .outPutString(bootstrap.getToStringService().taskToString(task));
                }
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public TaskGetListCommand() {
    }
}
