package ru.shumov.tm.command;


import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.project.Project;
import ru.shumov.tm.endpoint.user.Role;

import java.util.Collection;
import java.util.List;

public class ProjectGetListCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "project list";
    @Getter
    private final String description = "project list: Вывод всех проектов.";
    @SneakyThrows
    @Override
    public void execute() {
        int counter = 0;
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            @Nullable List<Project> values = bootstrap.getProjectEndPoint().getList(session);
            if (values.isEmpty()) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECTS_DO_NOT_EXIST);
                return;
            }
            for (Project project : values) {
                if (project.getUserId().equals(bootstrap.getUser().getId())) {
                    bootstrap.getTerminalService()
                            .outPutString(bootstrap.getToStringService().projectToString(project));
                }
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public ProjectGetListCommand() {
    }
}
