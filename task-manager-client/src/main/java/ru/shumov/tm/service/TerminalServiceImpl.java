package ru.shumov.tm.service;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.endpoint.project.Project;
import ru.shumov.tm.endpoint.task.Task;
import ru.shumov.tm.endpoint.user.User;

import java.util.Scanner;

public class TerminalServiceImpl implements TerminalService{

    Scanner scr = new Scanner(System.in);

    public String scanner() {
        String string = scr.nextLine();
        return string;
    }

    public void outPutString(@NotNull String string){
        System.out.println(string);
    }

    public void outPutProject(@NotNull Project project){
        System.out.println(project);
    }

    public void outPutTask(@NotNull Task task) {
        System.out.println(task);
    }

    public void outPutUser(@NotNull User user) {System.out.println(user);}
}
