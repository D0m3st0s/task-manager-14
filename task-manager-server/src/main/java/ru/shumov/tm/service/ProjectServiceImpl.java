package ru.shumov.tm.service;

import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.repository.factory.EntityManagerFactory;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.*;

public class ProjectServiceImpl implements ProjectService {
    private final EntityManagerFactory entityManagerFactory = new EntityManagerFactory();
    public void create(@NotNull Project project) {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project name");
            }
            manager.getTransaction().begin();
            manager.persist(project);
            manager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            manager.close();
        }
    }
    public void clear() {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Project> criteriaQuery = builder.createQuery(Project.class);

            Root<Project> root = criteriaQuery.from(Project.class);
            criteriaQuery.select(root);

            Query<Project> query = (Query<Project>) manager.createQuery(criteriaQuery);
            List<Project> projects = query.getResultList();
            for (Project project : projects) {
                manager.getTransaction().begin();
                manager.remove(project);
                manager.getTransaction().commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            manager.close();
        }
    }
    public void remove(@Nullable String projectId, String userId) {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            if (projectId == null || projectId.isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project id");
            }
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Task> criteriaQuery = builder.createQuery(Task.class);
            Root<Task> root = criteriaQuery.from(Task.class);
            criteriaQuery.select(criteriaQuery.from(Task.class)).where(builder.like(root.get("projectId"), projectId));
            Query<Task> query = (Query<Task>) manager.createQuery(criteriaQuery);
            List<Task> tasks = query.getResultList();
            for(Task task : tasks) {
                if(task.getProjectId().equals(projectId)) {
                    manager.getTransaction().begin();
                    manager.remove(task);
                    manager.getTransaction().commit();
                }
            }
            Project project = manager.find(Project.class, projectId);
            manager.getTransaction().begin();
            manager.remove(project);
            manager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            manager.close();
        }
    }
    public List<Project> getList(String id) {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Project> criteriaQuery = builder.createQuery(Project.class);

            Root<Project> root = criteriaQuery.from(Project.class);
            criteriaQuery.select(root).where(builder.equal(root.get("userId"), id));

            Query<Project> query = (Query<Project>) manager.createQuery(criteriaQuery);
            return query.getResultList();
        } finally {
            manager.close();
        }
    }
    public Project getOne(@NotNull String id, String userId) {
        Project project;
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            project = manager.find(Project.class, id);
            if(project.getUserId().equals(userId)) {
                return project;
            }
        } finally {
            manager.close();
        }
        return null;
    }
    public void update(@NotNull Project project) {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project name");
            }
            manager.getTransaction().begin();
            manager.merge(project);
            manager.getTransaction().commit();
        }
        catch (Exception e) {
            e.printStackTrace();
        } finally {
            manager.close();
        }
    }
    public List<Project> getSortedList(String id, String method) {
        List<Project> values;
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Project> criteriaQuery = builder.createQuery(Project.class);

            Root<Project> root = criteriaQuery.from(Project.class);
            criteriaQuery.select(root).where(builder.equal(root.get("userId"), id));

            Query<Project> query = (Query<Project>) manager.createQuery(criteriaQuery);
            values = query.getResultList();
            sorting(method, values);
        } finally {
            manager.close();
        }
        return values;
    }
    public List<Project> find(String id, String part){
        List<Project> projects = new ArrayList<>();
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Project> criteriaQuery = builder.createQuery(Project.class);

            Root<Project> root = criteriaQuery.from(Project.class);
            criteriaQuery.select(root).where(builder.equal(root.get("userId"), id));

            Query<Project> query = (Query<Project>) manager.createQuery(criteriaQuery);
            List<Project> values = query.getResultList();
            for (Project project : values) {
                if (project.getName().equals(part) || project.getDescription().equals(part)) {
                    projects.add(project);
                }
            }
        } finally {
            manager.close();
        }
        return projects;
    }
    private void sorting(@NotNull final String method, @NotNull final List<Project> projects) {
        switch (method) {
            case "by creating date":
                projects.sort(Comparator.comparing(Project::getCreatingDate));
                break;
            case "by start date":
                projects.sort(Comparator.comparing(Project::getStartDate));
                break;
            case "by end date":
                projects.sort(Comparator.comparing(Project::getEndDate));
                break;
            case "by status":
                projects.sort(Comparator.comparing(Project::getStatus));
                break;
        }
    }
}
