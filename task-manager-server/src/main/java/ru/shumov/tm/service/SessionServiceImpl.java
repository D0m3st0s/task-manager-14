package ru.shumov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;

import lombok.Setter;
import org.hibernate.query.Query;
import ru.shumov.tm.bootstrap.Bootstrap;
import ru.shumov.tm.entity.Session;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.exceptions.ValidateException;
import ru.shumov.tm.repository.factory.EntityManagerFactory;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.UUID;
@NoArgsConstructor
@Getter
@Setter
public class SessionServiceImpl implements SessionService{

    private final EntityManagerFactory entityManagerFactory = new EntityManagerFactory();
    private final String id = UUID.randomUUID().toString();
    private Bootstrap bootstrap;

    public SessionServiceImpl(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void validate(final Session session) throws ValidateException {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        if(session == null) throw new ValidateException();
        if(session.getSignature() == null || session.getSignature().isEmpty()) throw new ValidateException();
        if(session.getUserId() == null || session.getUserId().isEmpty()) throw new ValidateException();
        if(session.getTimestamp() == null) throw new ValidateException();
        final Session value = session.clone();
        final String sessionSignature = session.getSignature();
        final String valueSignature = sing(value).getSignature();
        final boolean check = sessionSignature.equals(valueSignature);
        try {
            if (!check) throw new ValidateException();
            Session sessionInDB = manager.find(Session.class, session.getId());
            manager.close();
            if (sessionInDB == null) throw new ValidateException();
        } catch (Exception ignored) {
        }
    }

    public Session sing(final Session session) {
        if(session == null) {return null;}
        session.setSignature(null);
        final String signature = SignatureService.sign(session, "Ништяк пацаны, сегодня кайфуем", 365);
        session.setSignature(signature);
        return session;
    }

    public Session openSession(final String login) throws ValidateException {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        Session finalSession;
        try {
            final User user = bootstrap.getUserService().getOne(login);
            final Session session = new Session();
            session.setUserId(user.getId());
            session.setOwnerId(id);
            session.setTimestamp(1000L);
            finalSession = sing(session);
            if (finalSession == null) throw new ValidateException();
            manager.getTransaction().begin();
            manager.persist(finalSession);
            manager.getTransaction().commit();
        } finally {
            manager.close();
        }
        return finalSession;
    }

    public void closeSession(final Session session) throws ValidateException {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            validate(session);
            manager.getTransaction().begin();
            manager.remove(session);
            manager.getTransaction().commit();
        } finally {
            manager.close();
        }
    }

    public List<Session> getSessionList() {
        List<Session> sessions;
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Session> criteriaQuery = builder.createQuery(Session.class);

            Root<Session> root = criteriaQuery.from(Session.class);
            criteriaQuery.select(root);

            Query<Session> query = (Query<Session>) manager.createQuery(criteriaQuery);
            sessions = query.getResultList();
        } finally {
            manager.close();
        }
        return sessions;
    }
}
