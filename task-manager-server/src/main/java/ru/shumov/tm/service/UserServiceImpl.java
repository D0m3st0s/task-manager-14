package ru.shumov.tm.service;

import lombok.SneakyThrows;
import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.repository.factory.EntityManagerFactory;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

public class UserServiceImpl implements UserService {
    private final EntityManagerFactory entityManagerFactory = new EntityManagerFactory();
    private final Md5Service md5Service;

    public UserServiceImpl(Md5Service md5Service) {
        this.md5Service = md5Service;
    }

    public List<User> getList() {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);

            Root<User> root = criteriaQuery.from(User.class);
            criteriaQuery.select(root);

            Query<User> query = (Query<User>) manager.createQuery(criteriaQuery);
            return query.getResultList();
        } finally {
            manager.close();
        }
    }

    public User getOne(@NotNull String login) {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            return manager.find(User.class, login);
        } finally {
            manager.close();
        }
    }

    public void create(@NotNull String login, String userPassword) {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        try {
            String password = md5Service.md5(userPassword);
            user.setPassword(password);
        }
        catch (NoSuchAlgorithmException e) {throw new RuntimeException(e);}
        user.setRole(Role.USER.toString());
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            manager.getTransaction().begin();
            manager.persist(user);
            manager.getTransaction().commit();
        } finally {
            manager.close();
        }
    }

    public void update(@NotNull User user) {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            manager.getTransaction().begin();
            manager.merge(user);
            manager.getTransaction().commit();
        } finally {
            manager.close();
        }
    }

    @SneakyThrows
    public void passwordUpdate(User user, String password) {
        var md5Password = md5Service.md5(password);
        user.setPassword(md5Password);
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            manager.getTransaction().begin();
            manager.merge(user);
            manager.getTransaction().commit();
        } finally {
            manager.close();
        }
    }
}
