package ru.shumov.tm.service;

import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.repository.factory.EntityManagerFactory;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskServiceImpl implements TaskService {
    private final EntityManagerFactory entityManagerFactory = new EntityManagerFactory();
    public void create(@NotNull Task task) {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "project id");
            }
            manager.getTransaction().begin();
            manager.persist(task);
            manager.getTransaction().commit();
        }
        catch (Exception e) {
            e.printStackTrace();
        } finally {
            manager.close();
        }
    }

    public void clear() {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Task> criteriaQuery = builder.createQuery(Task.class);

            Root<Task> root = criteriaQuery.from(Task.class);
            criteriaQuery.select(root);

            Query<Task> query = (Query<Task>) manager.createQuery(criteriaQuery);
            List<Task> tasks = query.getResultList();
            for (Task task : tasks) {
                manager.getTransaction().begin();
                manager.remove(task);
                manager.getTransaction().commit();
            }
        } finally {
            manager.close();
        }
    }

    public void remove(@NotNull String taskId) {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            manager.getTransaction().begin();
            manager.remove(manager.find(Task.class, taskId));
        } finally {
            manager.close();
        }
    }

    public List<Task> getList(@NotNull String id) {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Task> criteriaQuery = builder.createQuery(Task.class);

            Root<Task> root = criteriaQuery.from(Task.class);
            criteriaQuery.select(root).where(builder.equal(root.get("userId"), id));

            Query<Task> query = (Query<Task>) manager.createQuery(criteriaQuery);
            return query.getResultList();
        } finally {
            manager.close();
        }

    }

    public List<Task> getSortedList(String id, String method) {
        List<Task> tasks;
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Task> criteriaQuery = builder.createQuery(Task.class);

            Root<Task> root = criteriaQuery.from(Task.class);
            criteriaQuery.select(root).where(builder.equal(root.get("userId"), id));

            Query<Task> query = (Query<Task>) manager.createQuery(criteriaQuery);
            tasks = query.getResultList();
            sorting(method, tasks);
        } finally {
            manager.close();
        }
        return tasks;
    }

    public Task getOne(@NotNull String id, String userId) {
        Task task;
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            task = manager.find(Task.class, id);
            if(task.getUserId().equals(userId)) {
                return  task;
            }
        }finally {
            manager.close();
        }
        return null;
    }

    public List<Task> find(String id, String part) {
        List<Task> tasks = new ArrayList<>();
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Task> criteriaQuery = builder.createQuery(Task.class);

            Root<Task> root = criteriaQuery.from(Task.class);
            criteriaQuery.select(root).where(builder.equal(root.get("userId"), id));

            Query<Task> query = (Query<Task>) manager.createQuery(criteriaQuery);
            List<Task> values = query.getResultList();
            for (Task task : values) {
                if (task.getName().contains(part) || task.getDescription().contains(part)) {
                    tasks.add(task);
                }
            }
        } finally {
            manager.close();
        }
        return tasks;
    }

    public void update(@NotNull Task task) {
        EntityManager manager = entityManagerFactory.factory().createEntityManager();
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "project id");
            }
            manager.getTransaction().begin();
            manager.merge(task);
            manager.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            manager.close();
        }
    }
    public void sorting(@NotNull final String method, @NotNull final List<Task> tasks) {
        switch (method) {
            case "by creating date":
                tasks.sort(Comparator.comparing(Task::getCreatingDate));
                break;
            case "by start date":
                tasks.sort(Comparator.comparing(Task::getStartDate));
                break;
            case "by end date":
                tasks.sort(Comparator.comparing(Task::getEndDate));
                break;
            case "by status":
                tasks.sort(Comparator.comparing(Task::getStatus));
                break;
        }

    }
}
