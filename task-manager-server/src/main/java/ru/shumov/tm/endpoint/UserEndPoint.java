package ru.shumov.tm.endpoint;

import lombok.NoArgsConstructor;
import ru.shumov.tm.entity.Session;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.exceptions.EndpointException;
import ru.shumov.tm.service.SessionService;
import ru.shumov.tm.service.UserService;

import jakarta.jws.WebParam;
import jakarta.jws.WebService;


@NoArgsConstructor
@WebService
public class UserEndPoint {
    private SessionService sessionService;
    private UserService userService;

    public UserEndPoint(SessionService sessionService, UserService userService) {
        this.sessionService = sessionService;
        this.userService = userService;
    }

    public void create(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password) throws Exception{
        if(userService == null || sessionService == null) throw new EndpointException();
        userService.create(login, password);
    }
    public void passwordUpdate(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "user") final User user,
            @WebParam(name = "password") final String password) throws Exception {
        if(userService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        userService.passwordUpdate(user, password);
    }
    public User getOne(
            @WebParam(name = "login") final String login) throws Exception {
        if(userService == null || sessionService == null) throw new EndpointException();
        return userService.getOne(login);
    }
}
