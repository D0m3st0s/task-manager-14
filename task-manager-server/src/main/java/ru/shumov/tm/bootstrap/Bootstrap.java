package ru.shumov.tm.bootstrap;

import jakarta.xml.ws.Endpoint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.shumov.tm.endpoint.*;
import ru.shumov.tm.repository.*;
import ru.shumov.tm.service.*;

import java.sql.Connection;

@NoArgsConstructor
@Getter
@Setter
public class Bootstrap {

    private Connection connection;
    private final Md5Service md5Service = new Md5ServiceImpl();
    private final UserService userService = new UserServiceImpl(md5Service);
    private final TaskService taskService = new TaskServiceImpl();
    private final ProjectService projectService = new ProjectServiceImpl();
    private final SessionService sessionService = new SessionServiceImpl(this);

    public void main() {
        publishEndpoints();
        initConnection();
    }

    private void publishEndpoints(){
        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl",
                new ProjectEndPoint(projectService, sessionService));
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl",
                new TaskEndPoint(taskService, sessionService));
        Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl",
                new UserEndPoint(sessionService, userService));
        Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl",
                new SessionEndPoint(sessionService));

        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl");
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl");
        System.out.println("http://localhost:8080/UserEndpoint?wsdl");
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl");
    }

    public void initConnection() {
        this.connection = Connector.connectionDB();
    }
}
