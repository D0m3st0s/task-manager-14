package ru.shumov.tm;

import ru.shumov.tm.bootstrap.Bootstrap;

public class Application {
    public static void main(String[] args){
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.main();
    }
}
